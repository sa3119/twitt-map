package tweetData2.server;


//import javax.persistence.Entity;
//import javax.persistence.Id;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.*;

@Entity
public class TModelNew implements Serializable {

	/*
	 * Properties: TweetId, UserId, TweetText, Location, Time
	 */
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8844231070800300874L;
	@Id
	private Long tweetId;
	private String userId;
	private String tweetText;
	private String location;
	private String time;
	@Index private String keywords;
	
	/*
	 * Constructor
	 */
	
	public TModelNew() {
		//No arg constructor
	}
	public TModelNew(Long tweetId, String userId, String tweetText,
			String location, String time, String keywords) {
		super();
		this.tweetId = tweetId;
		this.userId = userId;
		this.tweetText = tweetText;
		this.location = location;
		this.time = time;
		this.keywords = keywords;
	}
	
	static { ObjectifyService.register(TModelNew.class); }

	public Long getTweetId() {
		return tweetId;
	}

	public void setTweetId(Long tweetId) {
		this.tweetId = tweetId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTweetText() {
		return tweetText;
	}

	public void setTweetText(String tweetText) {
		this.tweetText = tweetText;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	public String getKeywords() {
		return keywords;
	}
	
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	private void readObject(
		     ObjectInputStream aInputStream
		   ) throws ClassNotFoundException, IOException {
		     //always perform the default de-serialization first
		     aInputStream.defaultReadObject();
		  }
	
	private void writeObject(
		      ObjectOutputStream aOutputStream
		    ) throws IOException {
		      //perform the default serialization for all non-transient, non-static fields
		      aOutputStream.defaultWriteObject();
		    }
}
