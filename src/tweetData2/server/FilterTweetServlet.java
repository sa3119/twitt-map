package tweetData2.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.ObjectifyFactory;

import java.util.Collections;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;

public class FilterTweetServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1669698873494883373L;
	//Retrieve the tweets in the datastore based on the filter term selected by user
	public void doPost(HttpServletRequest req, HttpServletResponse res) {
		String selectedKeyword = req.getParameter("keyword");

		Cache cache;

		try {
			CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
			cache = cacheFactory.createCache(Collections.emptyMap());
			List<TModelNew> tweets = (List<TModelNew>) cache.get(selectedKeyword);

			ObjectifyFactory fact = new ObjectifyFactory(true);
			fact.register(TModelNew.class);
			
			
			
			if(tweets == null){
				tweets = ofy().load().type(TModelNew.class).filter("keywords", selectedKeyword).list();
				cache.put(selectedKeyword, tweets);
			}
			
			System.out.println("Executing query");
			int size = tweets.size();
			System.out.println(size);
			req.getSession().setAttribute("tweets", tweets);
		
		} catch (CacheException e) {
			e.printStackTrace();
		}
				
		try {
			res.sendRedirect("/map.jsp");
			//rd.forward(req, res);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
