package tweetData2.server;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tweetData2.client.GreetingService;
import tweetData2.shared.FieldVerifier;
import twitter4j.TwitterException;
import tweetData2.server.TwitterDownloader;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
		GreetingService {

	/*@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		      throws IOException { 
		//UserService userService = UserServiceFactory.getUserService();
	    //User user = userService.getCurrentUser();
	    
	    ArrayList<TModel> tmodels = null;
		try {
			tmodels = TwitterDownloader.download();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(TModel tweet : tmodels) {
		    Key modelKey = KeyFactory.createKey("TModel", "tweetskey");
		    Entity tweetModel = new Entity("TModel",modelKey);
		    tweetModel.setProperty("tweetText", tweet.getTweetText());
		    
		    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		    datastore.put(tweetModel);
		}

	}*/
	
	
	public String greetServer(String input) throws IllegalArgumentException {
		
		System.out.println("greetServer method called");
		
		System.out.println("create objectfactory");
		ObjectifyFactory fact = new ObjectifyFactory(true);
		System.out.println("register your class");
		fact.register(TModelNew.class);
		System.out.println("factory begin");
		Objectify ofy = fact.begin();
		
		System.out.println("Calling twitter downloader");
		/*ArrayList<TModelNew> tmodels = null;
		try {
			tmodels = TwitterDownloader.download();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(TModelNew tweet : tmodels) {
			ofy.save().entity(tweet).now();
		}*/
		
		
       /*   DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		FileReader fr = new FileReader("C:\\Users\\samsung\\workspace\\Twitt-trendy\\war\\data\\tweet_data.txt");
		BufferedReader br = new BufferedReader(fr);
		
		String line = "";
		int count = 0;
		//Read data from the file
		Key modelKey = KeyFactory.createKey("TModelNew", "newtweetskey");
		Entity tweetModel = new Entity("TModelNew",modelKey);
		//Key tm_key = tmodel.getKey();
		//System.out.println("Key has been created");
		//System.out.println(tm_key.toString());
		//Entity tweetModel = null;
		
		
		while((line = br.readLine()) != null) {
		    String[] attributes = line.split("~~");
		    count++;
		   if(attributes.length > 6) { 
		    tweetModel.setProperty("tweetId",attributes[0]);
		    tweetModel.setProperty("userId", attributes[1]);
		    tweetModel.setProperty("tweetText", attributes[2]);
		    tweetModel.setProperty("location", attributes[3]);
		    tweetModel.setProperty("time", attributes[4]);
		    tweetModel.setProperty("keywords", attributes[5]);
		   }
		    datastore.put(tweetModel);
		}
		br.close();*/
			
		// Verify that the input is valid. 
		if (!FieldVerifier.isValidName(input)) {
			// If the input is not valid, throw an IllegalArgumentException back to
			// the client.
			throw new IllegalArgumentException(
					"Name must be at least 4 characters long");
		}

		String serverInfo = getServletContext().getServerInfo();
		String userAgent = getThreadLocalRequest().getHeader("User-Agent");

		// Escape data from the client to avoid cross-site script vulnerabilities.
		input = escapeHtml(input);
		userAgent = escapeHtml(userAgent);

		return "Hello, " + input + "!<br><br>I am running " + serverInfo;
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}
}
