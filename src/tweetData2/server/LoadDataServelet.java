package tweetData2.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.ObjectifyFactory;

public class LoadDataServelet extends HttpServlet {
	
	/**
	 * Query the datastore to find the keywords
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) {
		ObjectifyFactory fact = new ObjectifyFactory(true);
		fact.register(TModelNew.class);
	/*	List<TModelNew> tweets = ofy().load().type(TModelNew.class).filter("keywords", "ICOTPOOL").list();
		HashMap<String,List<TModelNew>> keywordsHash = new HashMap<String,List<TModelNew>>();
		System.out.println("Executing query");
		int size = tweets.size();
		System.out.println(size);
		keywordsHash.put("ICOTPOOL", tweets);
		req.getSession().setAttribute("tweetsHash", keywordsHash);*/
		
		//List<TModelNew> allKeywords = ofy().load().type(TModelNew.class).filterKey("keywords").list();
		//req.getSession().setAttribute("allKeywords", allKeywords);
		
		List<TModelNew> allentities = ofy().load().type(TModelNew.class).list();
		HashSet<String> allKeywords = new HashSet<String>();
	
		for(TModelNew t : allentities) {
			String s = t.getKeywords();
			if(!allKeywords.contains(s)) {
				allKeywords.add(s);
			}
		}
		
		List<TModelNew> dummyTweets = new ArrayList<TModelNew>();
		req.getSession().setAttribute("allKeywords", allKeywords);
		req.getSession().setAttribute("tweets", dummyTweets);
		
		try {
			res.sendRedirect("/map.jsp");
			//rd.forward(req, res);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
