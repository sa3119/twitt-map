package tweetData2.server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import tweetData2.server.TModelNew;
import twitter4j.Location;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Trend;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

//import com.google.appengine.api.datastore.Key;
//import com.google.appengine.api.users.User;

public class TwitterDownloader {
	
	public static ArrayList<TModelNew> download() throws IOException, TwitterException {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		.setOAuthConsumerKey("tj2vrbvhRd3nVIx7AgJHTQoHd")  // Copy all the twitter API keys accordingly
		.setOAuthConsumerSecret("1vbVoDhXukzCCh4bRXNkzhT6D7K4c9pUR5udHIbzIMHVeLLbfP")
		.setOAuthAccessToken("120672631-ypvIu0c9IilJFh2yUZ72B1yJiPp6N869FHis9GvB")
		.setOAuthAccessTokenSecret("lYgn52Q4qfMCCW63h4ogS6Eokv5AYKV0y4Pbwp8AU2xrJ");
		
		Twitter twitter = new TwitterFactory(cb.build()).getInstance();
		
		/*RequestToken requestToken = twitter.getOAuthRequestToken();
	    AccessToken accessToken = null;
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    while (null == accessToken) {
	      System.out.println("Open the following URL and grant access to your account:");
	      System.out.println(requestToken.getAuthorizationURL());
	      System.out.print("Enter the PIN(if available) or just hit enter.[PIN]:");
	      String pin = br.readLine();
	      try{
	         if(pin.length() > 0){
	           accessToken = twitter.getOAuthAccessToken(requestToken, pin);
	           System.out.println("AccessToken :" + accessToken);
	           System.out.println("token" + accessToken.getToken());
	           System.out.println("secret" + accessToken.getTokenSecret());
	           
	         }else{
	           accessToken = twitter.getOAuthAccessToken();
	           System.out.println("Access token w/o pin" + accessToken);
	         }
	      } catch (TwitterException te) {
	        if(401 == te.getStatusCode()){
	          System.out.println("Unable to get the access token.");
	        }else{
	          te.printStackTrace();
	        }
	      }
	    }*/
		
		ResponseList<Location> locations = null;
		ArrayList<String> queryTopics = new ArrayList<String>(); 
		try {
			locations = twitter.getAvailableTrends();
			System.out.println("Getting reponse Locations ");
			
			for(Location loc: locations) {
				int woeid = loc.getWoeid();
				System.out.println("Country name: " + loc.getCountryName());
				System.out.println("PLace name: " + loc.getPlaceName());
				
				Trends trends = twitter.getPlaceTrends(woeid);
				System.out.println("Location: " + trends.getLocation());
				Trend[] t = trends.getTrends();
				for(Trend trend: t) {
					queryTopics.add(trend.getQuery());
				}
			}
		} catch(TwitterException te) {
			System.out.println(queryTopics.size() + " topics collected.");
		}
			
		/*ArrayList <TModel> TModelObjects = new ArrayList<TModel>();
		List<Status> statuses = twitter.getHomeTimeline();
	    System.out.println("Showing home timeline.");
	    for (Status status : statuses) {
	        System.out.println(status.getUser().getName() + ":" +
	                           status.getText());
	        TModel modelObj = new TModel(status.getId(), status.getUser().getScreenName(), status.getText(), status.getUser().getLocation(), status.getCreatedAt().toString());
			//ofy().save().entity(modelObj).now();
	        TModelObjects.add(modelObj);
	    }*/
		
		ArrayList <TModelNew> TModelObjects = new ArrayList<TModelNew>();
		for(String queryTerm: queryTopics) {
			
			String parsedQuery = queryTerm.replaceAll("[^a-zA-Z\\d\\s-_]", "");
			if(parsedQuery.startsWith("23") || parsedQuery.startsWith("22")) {
				parsedQuery = parsedQuery.substring(2);
			}
			if(parsedQuery.endsWith("23") || parsedQuery.endsWith("22")) {
				parsedQuery = parsedQuery.substring(0, parsedQuery.length()-2);
			}
			
			System.out.println("\n\n Query term : " + parsedQuery +"\n\n");
			Query query = new Query(queryTerm); // Search tweets for particular words
			query.setCount(20);
			
			QueryResult result;
			result = twitter.search(query);
			List<Status> tweets = result.getTweets();
			for (Status tweet : tweets) {
		    	
				if(tweet.getLang().equals("en")) {
					User myuser = tweet.getUser();
			    	String uname = myuser.getScreenName();
					String ulocation = myuser.getLocation();
					if(ulocation.equals("") || (ulocation == null)) {
						continue;
					}
					String text = tweet.getText();
					System.out.println("@" + uname + " - " + text + ": I am from " + ulocation);
					TModelNew modelObj = new TModelNew(tweet.getId(), uname, text, ulocation, tweet.getCreatedAt().toString(), parsedQuery);
					TModelObjects.add(modelObj);
				}
		    }
		}
		
		return TModelObjects;
	}

}
