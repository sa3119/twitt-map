<%@ page import="java.io.*,java.util.*,javax.servlet.*"%>
<%@ page import="tweetData2.server.TModelNew" %>
<%@ page import= "java.net.URL"%>
<%@ page import= "java.net.HttpURLConnection"%>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map-canvas { height: 80% }
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9AMdB-eQO_Lx8qs4c7sYDH0F9s4_k29Q&sensor=false&libraries=visualization">
    </script>
     <% 
     HashSet<String> allkeywords = (HashSet<String>)session.getAttribute("allKeywords");
	 //out.println("hashset size is " + allkeywords.size());  
	 %>
     
	</head>	 
    <body>
    
    <script type="text/javascript">
      
	 function initialize() {
 			var mapOptions = {
	          center: new google.maps.LatLng(42.3482, -75.1890),
	          zoom: 2
	    	};
			
			//the main map
        	var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
       
		<%-- var myJsonObj = { "results" : [ { "address_components" : [ { "long_name" : "Winnipeg", "short_name" : "Winnipeg", "types" : [ "locality", "political" ] }, { "long_name" : "Division No. 11", "short_name" : "Division No. 11", "types" : [ "administrative_area_level_2", "political" ] }, { "long_name" : "Manitoba", "short_name" : "MB", "types" : [ "administrative_area_level_1", "political" ] }, { "long_name" : "Canada", "short_name" : "CA", "types" : [ "country", "political" ] } ], "formatted_address" : "Winnipeg, MB, Canada", "geometry" : { "bounds" : { "northeast" : { "lat" : 49.9938741, "lng" : -96.9552281 }, "southwest" : { "lat" : 49.71377, "lng" : -97.349222 } }, "location" : { "lat" : 49.8997541, "lng" : -97.13749369999999 }, "location_type" : "APPROXIMATE", "viewport" : { "northeast" : { "lat" : 49.9938741, "lng" : -96.9552281 }, "southwest" : { "lat" : 49.71377, "lng" : -97.349222 } } }, "types" : [ "locality", "political" ] } ], "status" : "OK"}; --%>
      	}//initialize function ends
      google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    
    <!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&libraries=visualization"></script> -->    
    <div id="map-canvas"></div>  
    
    <div id="keywords">
		<form action="filter" method="POST">
			Select a keyword to filter the tweets:
			<select id="myList" name="keyword" onchange="this.form.submit()">
			  <option></option>
			  <%
			  Iterator<String> iter = allkeywords.iterator();
			  while(iter.hasNext()) {
			  %>
			  <option><%=iter.next()%></option>
			  <%
			  }
			  %>
			</select>
		</form>
    </div>
    
     <%
    String jsonObj = "";
    String allJsonObj = "[";
    List<TModelNew>tweets = (List<TModelNew>) session.getAttribute("tweets");
   
    if(tweets.size() >= 1) {    	
	   // out.println("Tweet size is " + tweets.size());	
	    for(int j = 0; j < tweets.size(); j++) {
	    		TModelNew t = tweets.get(j);
		    	String location = t.getLocation();
		    	String[] places = location.split(" ");
		    	String final_location = ""; 
		    	for(int i = 0; i <= (places.length-1); i++) {
		    		if(i == (places.length-1)) {
		    			final_location = final_location + places[i];
		    			continue;
		    		}
		    		final_location = final_location + places[i] + "+";	
		    	}
	    	
		    	URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address=" + final_location + "&sensor=false&key=AIzaSyC7v7BNYm2SqgbiEVGeqpAkeKY3vULWz24");
		    	HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		
				conn.setInstanceFollowRedirects(false); 
				conn.setRequestMethod("GET"); 
				conn.setRequestProperty("Content-Type", "application/json");
				InputStream is = conn.getInputStream(); 
		        BufferedReader rd = new BufferedReader(new InputStreamReader( is));
		
		        String line;
		        jsonObj = "";
		        while ((line = rd.readLine()) != null) 
		        {
		        	jsonObj = jsonObj + line;
		        }	        
		        rd.close();
		        if(j == tweets.size()-1) {
		        	allJsonObj = allJsonObj + jsonObj + "]";
		        }
		        else { allJsonObj = allJsonObj + jsonObj + ","; }
		        
		        int ResponseCode = conn.getResponseCode();
		        //out.println("STATUS CODE : " + ResponseCode);
		        //out.println(jsonObj);	
	    	}//outer for loop ends here
		    request.setAttribute("allJsonObj", allJsonObj);
    }//if size > 1 ends here
    
    //out.println("Tweets size is less than zero");
    //out.println("hello " + jsonObj);	
    %>
    
    <script type="text/javascript"> 
    
    function drawMarkers() {
    		var myJsonObj = <%=request.getAttribute("allJsonObj")%>;
   			
    		var mapOptions = {
    		          center: new google.maps.LatLng(42.3482, -75.1890),
    		          zoom: 2
    		    	};
    				
    				//the main map
    	    var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    		
   			//var i = 0;
   			var heatmapdata = [];
   			for(var i = 0;i < myJsonObj.length;i++) { 
   				
   				if(myJsonObj[i].status == "OK") {
   		        var lt = myJsonObj[i].results[0].geometry.location.lat;
   		        var ln = myJsonObj[i].results[0].geometry.location.lng;
   		        var latLn = new google.maps.LatLng(lt,ln);
   		       /*  marker = new google.maps.Marker({
   				      position: latLn,
   				      map: map,
   				      title: "this is my marker"
   				      
   				    }); */
   		        heatmapdata.push(latLn);
   				}//if OK ends
   			}
   			var heatmap = new google.maps.visualization.HeatmapLayer({ 
   				
   				data : heatmapdata,
   				dissipating : false,
   				map : map
   			});
    }
    google.maps.event.addDomListener(window, 'load', drawMarkers);
    </script>
    
    
    
  </body>
</html>

